import axios from "../axios-api";

export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const FETCH_LAST_MESSAGES_SUCCESS = "FETCH_LAST_MESSAGES_SUCCESS";
export const GET_MY_ID = "GET_MY_ID";
export const CATCH_ERROR = "CATCH_ERROR";

export const fetchMessagesSuccess = data => {
  return { type: FETCH_MESSAGES_SUCCESS, data };
};

export const getPartOfMessages = () => {
  return dispatch => {
    return axios
      .get("/messages")
      .then(response => dispatch(fetchMessagesSuccess(response.data)));
  };
};

export const getMyId = id => {
  return { type: GET_MY_ID, id };
};

export const catchError = error => {
  return { type: CATCH_ERROR, error };
};

export const postMessage = data => {
  return dispatch => {
    return axios.post("/messages", data).then(
      message => {
        dispatch(getMyId(message.data.id));
      },
      error => dispatch(catchError(JSON.stringify(error.response.data)))
    );
  };
};

export const fetchLastMessagesSuccess = data => {
  return { type: FETCH_LAST_MESSAGES_SUCCESS, data };
};

export const getLastMessages = datetime => {
  return dispatch => {
    return axios
      .get(`/messages?datetime=${datetime}`)
      .then(response => dispatch(fetchLastMessagesSuccess(response.data)));
  };
};
