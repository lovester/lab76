import {
  CATCH_ERROR,
  FETCH_LAST_MESSAGES_SUCCESS,
  FETCH_MESSAGES_SUCCESS,
  GET_MY_ID
} from "./actions";

const initialState = {
  messages: [],
  myId: [],
  errors: ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_SUCCESS:
      return { ...state, messages: action.data };
    case GET_MY_ID:
      return { ...state, myId: state.myId.concat(action.id) };
    case FETCH_LAST_MESSAGES_SUCCESS:
      return { ...state, messages: state.messages.concat(action.data) };
    case CATCH_ERROR:
      return { ...state, errors: action.error };
    default:
      return state;
  }
};

export default reducer;
