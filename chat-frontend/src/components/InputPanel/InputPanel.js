import React from "react";
import "./InputPanel.css";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/es/Button/Button";
import Input from "@material-ui/core/es/Input/Input";
import { FormHelperText } from "@material-ui/core";

const InputPanel = ({ change, postMessage, error }) => {
  return (
    <div className="InputPanel">
      <FormControl>
        <Input
          id="name-input"
          className="Input Input-name"
          placeholder="Name"
          onChange={event => change(event)}
          name="author"
        />
        {!!error ? (
          <FormHelperText style={{ color: "red" }}>
            {JSON.parse(error).errorName}
          </FormHelperText>
        ) : null}
      </FormControl>
      <FormControl>
        <Input
          id="name-input"
          placeholder="Message"
          className="Input Input-message"
          name="message"
          onChange={event => change(event)}
        />
        {!!error ? (
          <FormHelperText style={{ color: "red" }}>
            {JSON.parse(error).errorMessage}
          </FormHelperText>
        ) : null}
      </FormControl>
      <Button variant="contained" size="small" onClick={postMessage}>
        Send
      </Button>
    </div>
  );
};

export default InputPanel;
