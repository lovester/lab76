import React from 'react';
import './ViewPanel.css';

const isMyMessage = (myMessagesId, id) => {

  for (let i = 0; i < myMessagesId.length; i++) {
    if (myMessagesId[i] === id) {
      return "ViewPanel my-message"
    }
  }
  return "ViewPanel";
};

const ViewPanel = ({messages, myMessagesId}) => {
  return(
    <div className='Messages'>
      { messages.map((item, index) => {
        return (
          <div key={index} className={isMyMessage(myMessagesId, item.id)}>
            [ {item.author} ]: {item.message}
          </div>  )
      })}
    </div>
  )
};
//
export default ViewPanel;