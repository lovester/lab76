import React, { Component } from "react";
import "./Chat.css";
import InputPanel from "../components/InputPanel/InputPanel";
import ViewPanel from "../components/ViewPanel/ViewPanel";
import { connect } from "react-redux";
import {
  getLastMessages,
  getPartOfMessages,
  postMessage
} from "../store/actions";

class Chat extends Component {
  state = {
    message: "",
    author: ""
  };

  interval = null;

  onChangeHandle = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  takeInterval = () => {
    this.interval = setInterval(() => {
      const lastDatetime = this.props.messages[this.props.messages.length - 1]
        .datetime;
      this.props.onGetLastMessages(lastDatetime).then(this.scrollToBottom);
    }, 15000);
  };

  scrollToBottom = () => {
    const element = document.getElementsByClassName("Messages")[0];
    element.scrollTop = element.scrollHeight;
  };

  makeDataToPost = () => {
    return {
      author: this.state.author,
      message: this.state.message
    };
  };

  componentDidMount() {
    this.props.onGetPartOfMessages().then(this.takeInterval());
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div className="App">
        <ViewPanel
          messages={this.props.messages}
          myMessagesId={this.props.myId}
        />
        <InputPanel
          change={this.onChangeHandle}
          postMessage={() => this.props.onPostMessage(this.makeDataToPost())}
          error={this.props.error}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messages,
    myId: state.myId,
    error: state.errors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetPartOfMessages: () => dispatch(getPartOfMessages()),
    onPostMessage: messageData => dispatch(postMessage(messageData)),
    onGetLastMessages: lastDatetime => dispatch(getLastMessages(lastDatetime))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
