const express = require('express');
const fs = require('fs');
const fileDb = require('../FileDb');
const nanoId = require('nanoid');

const chat = express.Router();

chat.post('/messages', (req, res) => {
  if (req.body.author === '' || req.body.message === '') {
    res.send({"error": "Author and message must be present in the request"})
  } else {
    req.body.id = nanoId();
    req.body.datetime = new Date();
    res.send(fileDb.addItem(req.body));
  }
});

chat.get('/messages', (req, res) => {
  const messageList = fs.readFileSync('./db.json');
  const messages = [];

  const date = new Date(req.query.datetime);
  if (isNaN(date.getDate()) && req.query.datetime) {
    return res.status(400).send('Invalid date forman');
  }

  JSON.parse(messageList).slice(-30).map(message => {
    if (req.query.datetime) {
      if (new Date(message.datetime) > new Date(req.query.datetime)) messages.push(message)
    } else {
      messages.push(message);
    }
  });
  res.send(messages);
});

module.exports = chat;