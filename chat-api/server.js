const express = require('express');
const chat = require('./app/chat');
const fileDb = require('./FileDb');
const cors = require('cors');
const app = express();
fileDb.init();

app.use(express.json());
app.use(cors());
app.use(chat);

const port = 8000;

app.listen(port, () => {
    console.log(`Server starts on ${port} port`);
});
